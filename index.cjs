const keys_que = require("./keys.cjs");
const Obj = require("./testData.cjs");
const values_que = require("./values.cjs");
const pairs_que = require("./pairs.cjs");
const invert_que = require("./invert.cjs");
const mapObj = require("./mapObject.cjs");

mapObj(Obj);

// console.log(keys_que(Obj));

// console.log(values_que(Obj));

// console.log(pairs_que(Obj));

// console.log(invert_que(Obj));
