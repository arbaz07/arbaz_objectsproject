const _ = require("underscore");
//const testData = require("./testData.cjs");

function pairs(obj){
    if(obj == null){
        return [];
    }
    if(typeof(obj)=='object'){
        return _.pairs(obj);
    }
    return [];
}

module.exports = pairs;