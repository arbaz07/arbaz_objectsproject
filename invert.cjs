const _ = require("underscore");

function invert(obj){
    if(obj == null){
        return {};
    }
    if(typeof(obj)=='object'){
        return _.invert(obj);
    }
    return {};
}

module.exports = invert;