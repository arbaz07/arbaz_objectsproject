const obj = require("./testData.cjs")
let _ = require('underscore');


function values(obj){
    if(obj == null){
        return [];
    }
    if(typeof(obj)=='object'){
        return _.values(obj);
    }
    return [];
}

module.exports = values;